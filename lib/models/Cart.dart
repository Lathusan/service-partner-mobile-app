import 'package:flutter/material.dart';

import 'Product.dart';
import 'Product.dart';

class Cart {
  final Product product;
  final int numOfItem;

  Cart({@required this.product, @required this.numOfItem});
}

List<Cart> demoCarts = [
  Cart(product: demoServices[0], numOfItem: 2),
  Cart(product: demoServices[1], numOfItem: 1),
  Cart(product: demoServices[3], numOfItem: 1),
];
